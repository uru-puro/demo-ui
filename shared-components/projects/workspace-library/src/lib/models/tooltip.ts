import { TooltipPosition } from "@angular/material/tooltip";

export interface Tooltip {
    position: TooltipPosition,
    tooltipClass: string,
}
