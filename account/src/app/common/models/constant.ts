export const ROUTES = {
    AUTH: {
        LOGIN: 'login',
        FORGOT_PASSWORD: 'forgot-password',
        ABSOLUTE_LOGIN: '/login',
    }
};
