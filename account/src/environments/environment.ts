export const ENVIRONMENT = {
    production: false,
    HOST_ENDPOINT: "https://dev-apigateway.api.workspace.anasource.com",
    AUTH_ENDPOINT: "https://dev-apigateway.api.workspace.anasource.com",
    PMS_CLIENT_URL: "https://qa.workspace.anasource.com/"
};
