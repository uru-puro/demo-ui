import { Component } from '@angular/core';

@Component({
  selector: 'app-selectdropdown',
  templateUrl: './selectdropdown.component.html',
  styleUrl: './selectdropdown.component.css'
})
export class SelectdropdownComponent {
  selected = 'option1';
}
