export const AttendanceTypeLabels = [
    {
        attendanceType: "Full Leave",
        className: "full-leave-box"
    },
    {
        attendanceType: "Half Leave",
        className: "half-leave-box"
    },
    {
        attendanceType: "Today",
        className: "today-box"
    },
    {
        attendanceType: "Weekend",
        className: "weekend-box"
    },
    {
        attendanceType: "Holiday",
        className: "holiday-box"
    }
];
