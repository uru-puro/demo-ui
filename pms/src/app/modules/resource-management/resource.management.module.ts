import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResourceManagementRoutingModule } from './resource.management.routing.module';

@NgModule({
  imports: [
    CommonModule,
    ResourceManagementRoutingModule
  ]
})
export class ResourceManagementModule { }
