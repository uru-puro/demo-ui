import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PresalesRoutingModule } from './presales-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PresalesRoutingModule
  ]
})
export class PresalesModule { }
