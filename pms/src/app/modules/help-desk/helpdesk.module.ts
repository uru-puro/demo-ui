import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HelpDeskRoutingModule } from "./helpdesk-routing.module";

@NgModule({
    declarations: [],
    imports: [
      CommonModule,
      HelpDeskRoutingModule
    ]
  })
  export class HelpDeskModule { }
