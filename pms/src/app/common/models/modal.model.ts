export interface ModalConfig {
    size: string;
    title: string;
}

export interface TechnicalSkillModalData {
    title: string;
    id:number;
    fullName?: string;
    technicalSkillIds: number[];
}
